package net.sample.dataloadorchestrator.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.sample.dataloadorchestrator.model.RetryFileRequest;
import net.sample.dataloadorchestrator.response.RetryFileResponse;
import net.sample.dataloadorchestrator.service.DummyRetryFileService;
import net.sample.dataloadorchestrator.service.RetryFileInterface;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@EnableWebMvc
@WebMvcTest(RetryFileController.class)
class RetryFileServiceBusControllerTest {
    @Autowired
    private MockMvc mockMvc;

    private final DummyRetryFileService dummyRetryFileService = new DummyRetryFileService();

    @MockBean
    RetryFileInterface retryFileInterface;

    private final String uri = "/api/retry-files";
    private final String buildName = "local-build";
    private final String entityName = "Email";
    private final String filename = "email.csv";

    @BeforeEach
    public void initRetryFileService() {
        doAnswer((InvocationOnMock invocation) -> {
            RetryFileRequest param = invocation.getArgument(0);
            return dummyRetryFileService.retryFiles(param);
        })
                .when(retryFileInterface)
                .retryFiles(any(RetryFileRequest.class));
    }

    @Test
    void testGetBuildValue() throws Exception {
        RetryFileResponse retryFileResponse = new RetryFileResponse();
        retryFileResponse.setBuild(buildName);
        retryFileResponse.setEntity(entityName);

        List<String> filenames = new ArrayList<>();
        filenames.add(filename);
        RetryFileRequest retryFileRequest = new RetryFileRequest(buildName, entityName, filenames);
        ObjectMapper objectMapper = new ObjectMapper();
        String retryFileRequestJSON = objectMapper.writeValueAsString(retryFileRequest);

        ResultActions result = mockMvc.perform(
                MockMvcRequestBuilders
                        .post(uri)
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .content(retryFileRequestJSON));

        result.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.build").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.build").value(buildName));
    }

    @Test
    void testGetEntityValue() throws Exception {
        RetryFileResponse retryFileResponse = new RetryFileResponse();
        retryFileResponse.setBuild(buildName);
        retryFileResponse.setEntity(entityName);

        List<String> filenames = new ArrayList<>();
        filenames.add(filename);
        RetryFileRequest retryFileRequest = new RetryFileRequest(buildName, entityName, filenames);
        ObjectMapper objectMapper = new ObjectMapper();
        String retryFileRequestJSON = objectMapper.writeValueAsString(retryFileRequest);

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .accept(MediaType.APPLICATION_JSON_VALUE)
                                .content(retryFileRequestJSON)
                ).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.entity").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.entity").value(entityName));
    }

    @Test
    void testCorrectRequest() throws Exception {
        List<String> filenames = new ArrayList<>();
        filenames.add(filename);
        RetryFileRequest retryFileRequest = new RetryFileRequest(buildName, entityName, filenames);
        ObjectMapper objectMapper = new ObjectMapper();
        String retryFileRequestJSON = objectMapper.writeValueAsString(retryFileRequest);

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(retryFileRequestJSON)
        ).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    }

    @Test
    void testInvalidContentType() throws Exception {
        List<String> filenames = new ArrayList<>();
        filenames.add(filename);
        RetryFileRequest retryFileRequest = new RetryFileRequest(buildName, entityName, filenames);
        ObjectMapper objectMapper = new ObjectMapper();
        String retryFileRequestJSON = objectMapper.writeValueAsString(retryFileRequest);

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_XML)
                        .accept(MediaType.APPLICATION_XML)
                        .content(retryFileRequestJSON)
        ).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(415, status);
    }

    @Test
    void testInvalidURL() throws Exception {
        List<String> filenames = new ArrayList<>();
        filenames.add(filename);
        RetryFileRequest retryFileRequest = new RetryFileRequest(buildName, entityName, filenames);
        ObjectMapper objectMapper = new ObjectMapper();
        String retryFileRequestJSON = objectMapper.writeValueAsString(retryFileRequest);

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/anyOtherPath")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(retryFileRequestJSON)
        ).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

}
