package net.sample.dataloadorchestrator.api;


import net.sample.dataloadorchestrator.model.DataLoadState;
import net.sample.dataloadorchestrator.model.DataLoadSummary;
import net.sample.dataloadorchestrator.model.FileSummary;
import net.sample.dataloadorchestrator.response.BuildSummary;
import net.sample.dataloadorchestrator.service.BuildService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BuildController.class)
class BuildControllerTest {
    private static final String BUILD = "build";
    private static final String TEST_UUID = "454fb664-a933-4e1b-a2c8-41db681c77c4";
    private static final String ENTITY = "entity";

    @MockBean
    private BuildService buildService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getBuildNotFound() throws Exception {
        when(buildService.getBuildSummary(BUILD)).thenReturn(null);
        mockMvc.perform(get("/api/builds/{buildName}", BUILD))
                .andExpect(status().isNotFound());
    }

    @Test
    void getBuild() throws Exception {
        BuildSummary buildSummary = new BuildSummary();
        buildSummary.setBuild(BUILD);
        when(buildService.getBuildSummary(BUILD))
                .thenReturn(buildSummary);
        mockMvc.perform(get("/api/builds/{buildName}", BUILD))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.build").value(BUILD));
    }

    @Test
    void getEntityFiles() throws Exception {
        DataLoadSummary dataLoadSummary = new DataLoadSummary();
        dataLoadSummary.setBuild(BUILD);
        List<FileSummary> fileSummaries = new ArrayList<>();
        FileSummary fileSummary = new FileSummary(
                "Bucket",
                "Path",
                1L,
                DataLoadState.DONE,
                LocalDateTime.now(),
                LocalDateTime.now());
        fileSummaries.add(fileSummary);
        dataLoadSummary.setFiles(fileSummaries);
        when(buildService.getEntityFiles(BUILD, ENTITY, null))
                .thenReturn(dataLoadSummary);
        mockMvc.perform(get("/api/builds/{buildName}/entities/{entity}/files", BUILD, ENTITY))
                .andExpect(status().isOk());
    }

    @Test
    void getEntityFileSummary() throws Exception {
        FileSummary fileSummary = new FileSummary(
                "Bucket",
                "Path",
                1L,
                DataLoadState.DONE,
                LocalDateTime.now(),
                LocalDateTime.now());
        UUID uuid = UUID.fromString(TEST_UUID);
        when(buildService.getFileSummary(BUILD, ENTITY, uuid))
                .thenReturn(fileSummary);
        mockMvc.perform(get("/api/builds/{buildName}/entities/{entity}/files/{fileId}",
                        BUILD,
                        ENTITY,
                        uuid))
                .andExpect(status().isOk());
    }

}
