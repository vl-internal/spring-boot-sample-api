package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@DataJpaTest
public class DataLoadInfoRepositoryTest {
    @Autowired
    private DataLoadInfoRepository dataLoadInfoRepository;

    @Test
    void testReadAndWrite() {
        final var dataLoadInfo = new DataLoadInfo();
        dataLoadInfo.setBucket("bucket");
        dataLoadInfo.setFilePath("filePath");

        dataLoadInfoRepository.save(dataLoadInfo);

        final var results = dataLoadInfoRepository.findAll();
        assertEquals(1, dataLoadInfoRepository.findAll().size());
        dataLoadInfoRepository.findAll().stream().findFirst().ifPresentOrElse(
                result -> {
                    assertEquals(dataLoadInfo.getBucket(), result.getBucket());
                    assertEquals(dataLoadInfo.getFilePath(), result.getFilePath());
                },
                () -> fail("Could not find requested entity")
        );
    }
}
