package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.time.LocalDateTime;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@DataJpaTest

class BuildInfoRepositoryTest {

    @Autowired
    private BuildInfoRepository buildInfoRepository;

    @Autowired private DataSource dataSource;
    @Autowired private JdbcTemplate jdbcTemplate;
    @Autowired private EntityManager entityManager;


    @Test
    void injectedComponentsAreNotNull(){
        assertThat(dataSource).isNotNull();
        assertThat(jdbcTemplate).isNotNull();
        assertThat(entityManager).isNotNull();
        assertThat(buildInfoRepository).isNotNull();
    }

    @Test
    void testReadAndWrite() {
        var id = "mock";
        var info = new BuildInfo(id, true, LocalDateTime.now());

        buildInfoRepository.save(info);

        buildInfoRepository.findById(id).ifPresentOrElse(
                result -> {
                    assertEquals(info.getBuild(), result.getBuild());
                    assertEquals(info.getCompletedFileDate(), result.getCompletedFileDate());
                    assertEquals(info.isCompleteFilePresent(), result.isCompleteFilePresent());
                },
                () -> fail("Could not find requested entity")
        );
    }
}
