package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@DataJpaTest
public class DataLoadSettingsRepositoryTest {
    @Autowired
    private DataLoadSettingsRepository dataLoadSettingsRepository;

    @Test
    void testReadAndWrite() {
        final var entity = "entity";
        final var setting = new DataLoadSetting();
        setting.setEntity(entity);
        setting.setLoadingEnabled(Boolean.TRUE);

        dataLoadSettingsRepository.save(setting);

        dataLoadSettingsRepository.findById(entity).ifPresentOrElse(
            result -> {
                assertEquals(entity, result.getEntity());
                assertEquals(setting.getLoadingEnabled(), result.getLoadingEnabled());
            },
            () -> fail("Could not find requested entity")
        );
    }
}
