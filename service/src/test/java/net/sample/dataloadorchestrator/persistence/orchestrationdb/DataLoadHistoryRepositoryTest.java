package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import net.sample.dataloadorchestrator.model.DataLoadState;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@DataJpaTest
public class DataLoadHistoryRepositoryTest {
    @Autowired
    private DataLoadHistoryRepository dataLoadHistoryRepository;

    @Test
    void testReadAndWrite() {
        final var build = "build";
        final var entity = "entity";

        final var dataLoadHistory = new DataLoadHistory();
        dataLoadHistory.setBuild(build);
        dataLoadHistory.setEntity(entity);
        dataLoadHistory.setState(DataLoadState.LOAD_COMPLETED);

        dataLoadHistoryRepository.save(dataLoadHistory);

        dataLoadHistoryRepository.findById(new DataLoadHistoryId(build, entity)).ifPresentOrElse(
            result -> {
                assertEquals(dataLoadHistory.getBuild(), result.getBuild());
                assertEquals(dataLoadHistory.getEntity(), result.getEntity());
                assertEquals(dataLoadHistory.getState(), result.getState());
            },
            () -> fail("Could not find requested entity")
        );
    }
}
