package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@DataJpaTest
public class DataLoadMetricsRepositoryTest {
    @Autowired
    private DataLoadMetricsRepository dataLoadMetricsRepository;

    @Test
    void testReadAndWrite() {
        final var id = UUID.randomUUID();
        final var metrics = new DataLoadMetrics(id, 1, 1);

        dataLoadMetricsRepository.save(metrics);

        assertEquals(1, dataLoadMetricsRepository.findAll().size());
        dataLoadMetricsRepository.findAll().stream().findFirst().ifPresentOrElse(
                result -> {
                    assertEquals(metrics.getId(), result.getId());
                    assertEquals(metrics.getFileInfoId(), result.getFileInfoId());
                    assertEquals(metrics.getRecordsSucceeded(), result.getRecordsSucceeded());
                    assertEquals(metrics.getRecordsFailed(), result.getRecordsFailed());
                },
                () -> fail("Could not find requested entity")
        );
    }
}
