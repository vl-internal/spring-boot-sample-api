package net.sample.dataloadorchestrator.mapper;

import net.sample.dataloadorchestrator.model.DataLoadState;
import net.sample.dataloadorchestrator.model.FileSummary;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.DataLoadInfo;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileSummaryMapperTest {
    private static final long FILE_SIZE = 10L;
    private static final String BUCKET = "bucket";
    private static final String FILE_PATH = "my/file";
    private static final DataLoadState STATE = DataLoadState.DONE;
    private static final LocalDateTime STARTED_DATE = LocalDateTime.of(2021, 04, 26, 0, 0, 0);
    private static final LocalDateTime FINISH_DATE = LocalDateTime.of(2021, 04, 26, 1, 0, 0);

    private static final FileSummaryMapper MAPPER = new FileSummaryMapper();

    @Test
    void testMap_Bucket() {
        final FileSummary result = MAPPER.map(getLoadInfo());
        assertEquals(BUCKET, result.getBucket());
    }

    @Test
    void testMap_FilePath() {
        final FileSummary result = MAPPER.map(getLoadInfo());
        assertEquals(FILE_PATH, result.getPath());
    }

    @Test
    void testMap_FileSize() {
        final FileSummary result = MAPPER.map(getLoadInfo());
        assertEquals(FILE_SIZE, result.getSizeInBytes());
    }

    @Test
    void testMap_State() {
        final FileSummary result = MAPPER.map(getLoadInfo());
        assertEquals(STATE, result.getState());
    }

    @Test
    void testMap_DateCreated() {
        final FileSummary result = MAPPER.map(getLoadInfo());
        assertEquals(STARTED_DATE, result.getStartDate());
    }

    @Test
    void testMap_DateUpdated() {
        final FileSummary result = MAPPER.map(getLoadInfo());
        assertEquals(FINISH_DATE, result.getLastUpdatedDate());
    }

    private DataLoadInfo getLoadInfo() {
        final DataLoadInfo dataLoadInfo = new DataLoadInfo();
        dataLoadInfo.setBucket(BUCKET);
        dataLoadInfo.setFilePath(FILE_PATH);
        dataLoadInfo.setFileSize(FILE_SIZE);
        dataLoadInfo.setState(STATE);
        dataLoadInfo.setDateCreated(STARTED_DATE);
        dataLoadInfo.setDateUpdated(FINISH_DATE);
        return dataLoadInfo;
    }
}
