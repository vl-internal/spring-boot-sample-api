package net.sample.dataloadorchestrator.mapper;

import net.sample.dataloadorchestrator.model.DataLoadState;
import net.sample.dataloadorchestrator.model.DataLoadSummary;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.DataLoadHistory;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DataLoadSummaryMapperTest {

    private static final String BUILD = "build";
    private static final String ENTITY = "entity";
    private static final String COLOR = "blue";
    private static final DataLoadState STATE = DataLoadState.DONE;
    private static final LocalDateTime STARTED_DATE = LocalDateTime.of(2021, 04, 26, 0, 0, 0);
    private static final LocalDateTime FINISH_DATE = LocalDateTime.of(2021, 04, 26, 1, 0, 0);

    private static final DataLoadSummaryMapper MAPPER = new DataLoadSummaryMapper();

    @Test
    void testMap_Build() {
        final DataLoadSummary result = MAPPER.map(getDataLoadHistory());
        assertEquals(BUILD, result.getBuild());
    }

    @Test
    void testMap_Entity() {
        final DataLoadSummary result = MAPPER.map(getDataLoadHistory());
        assertEquals(ENTITY, result.getEntity());
    }

    @Test
    void testMap_Color() {
        final DataLoadSummary result = MAPPER.map(getDataLoadHistory());
        assertEquals(COLOR, result.getTargetColor());
    }

    @Test
    void testMap_Status() {
        final DataLoadSummary result = MAPPER.map(getDataLoadHistory());
        assertEquals(STATE, result.getState());
    }

    @Test
    void testMap_StartedDate() {
        final DataLoadSummary result = MAPPER.map(getDataLoadHistory());
        assertEquals(STARTED_DATE, result.getStartedDate());
    }

    @Test
    void testMap_FinishDate() {
        final DataLoadSummary result = MAPPER.map(getDataLoadHistory());
        assertEquals(FINISH_DATE, result.getCompletedDate());
    }

    private DataLoadHistory getDataLoadHistory() {
        final DataLoadHistory loadHistory = new DataLoadHistory();
        loadHistory.setBuild(BUILD);
        loadHistory.setEntity(ENTITY);
        loadHistory.setEntityTargetColor(COLOR);
        loadHistory.setState(STATE);
        loadHistory.setStartedDate(STARTED_DATE);
        ;
        loadHistory.setFinishedDate(FINISH_DATE);
        loadHistory.setFiles(Collections.emptyList());
        return loadHistory;
    }
}
