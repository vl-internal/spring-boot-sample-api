package net.sample.dataloadorchestrator.mapper;

import net.sample.dataloadorchestrator.model.DataLoadEntity;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.DataLoadSetting;
import net.sample.dataloadorchestrator.response.DataLoadSettingSummary;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
public class DataLoadSettingsMapperTest {

    private static final DataLoadSettingsMapper MAPPER = new DataLoadSettingsMapper();
    public static final DataLoadEntity EMAIL = DataLoadEntity.EMAIL;
    public static final Boolean TRUE = Boolean.TRUE;

    @Test
    void testMapEntity() {
        DataLoadSettingSummary summary = MAPPER.map(getDataLoadSetting());
        assertTrue(summary.getEntity().equals(EMAIL.name()));
    }
    
    @Test
    void testMapLoadingEnabled() {
        DataLoadSettingSummary summary = MAPPER.map(getDataLoadSetting());
        assertTrue(summary.getLoadingEnabled().equals(TRUE));
    }
    

    private DataLoadSetting getDataLoadSetting() {
        DataLoadSetting setting = new DataLoadSetting();
        setting.setEntity(EMAIL.name());
        setting.setLoadingEnabled(TRUE);
        return setting;
    }
}
