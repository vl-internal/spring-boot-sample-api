package net.sample.dataloadorchestrator.mapper;

import net.sample.dataloadorchestrator.model.DataLoadState;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.BuildInfo;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BuildSummaryMapperTest {
    private static final String BUILD = "build";
    private static final boolean COMPLETED = Boolean.TRUE;

    private static final BuildSummaryMapper MAPPER = new BuildSummaryMapper();

    @Test
    void testMap() {
        final var result = MAPPER.map(getBuildInfo(null));
        assertEquals(BUILD, result.getBuild());
        assertEquals(DataLoadState.LOADING, result.getState());
    }

    @Test
    void testMapDoneState() {
        final var result = MAPPER.map(getBuildInfo(LocalDateTime.now()));
        assertEquals(BUILD, result.getBuild());
        assertEquals(DataLoadState.DONE, result.getState());
    }

    private BuildInfo getBuildInfo(final LocalDateTime date) {
        return new BuildInfo(BUILD, COMPLETED, date);
    }
}
