package net.sample.dataloadorchestrator.service;


import net.sample.dataloadorchestrator.mapper.DataLoadSettingsMapper;
import net.sample.dataloadorchestrator.model.DataLoadEntity;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.DataLoadSetting;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.DataLoadSettingsRepository;
import net.sample.dataloadorchestrator.response.DataLoadSettingSummary;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DataLoadSettingsServiceTest {

    @Mock(answer= Answers.CALLS_REAL_METHODS)
    private DataLoadSettingsMapper mapper;

    @Mock
    private DataLoadSettingsRepository dataLoadSettingsRepository;

    @InjectMocks
    private DataLoadSettingsService dataLoadSettingsService;

    private static final DataLoadSettingSummary GOOD_SUMMARY = new DataLoadSettingSummary("ABCD", Boolean.TRUE);
    private static final DataLoadSettingSummary BAD_SUMMARY = new DataLoadSettingSummary("BAD", Boolean.TRUE);

    @Test
    void testList() {
        final DataLoadSetting setting = new DataLoadSetting();
        setting.setEntity(DataLoadEntity.ABCD.name());
        setting.setLoadingEnabled(Boolean.TRUE);
        when(dataLoadSettingsRepository.findAll()).thenReturn(List.of(setting));
        assertEquals(1, dataLoadSettingsService.getDataLoadSettingSummaries().size());
    }

    @Test
    void testUpdateSuccess(){
        assertDoesNotThrow(() -> dataLoadSettingsService.saveDataLoadSetting(GOOD_SUMMARY));
    }

    @Test
    void testUpdateFailure(){
        assertThrows(IllegalArgumentException.class, () -> {
            dataLoadSettingsService.saveDataLoadSetting(BAD_SUMMARY);
        });
    }


}
