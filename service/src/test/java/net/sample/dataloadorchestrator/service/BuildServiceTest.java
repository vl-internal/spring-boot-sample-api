package net.sample.dataloadorchestrator.service;

import net.sample.dataloadorchestrator.mapper.BuildSummaryMapper;
import net.sample.dataloadorchestrator.mapper.DataLoadSummaryMapper;
import net.sample.dataloadorchestrator.model.DataLoadState;
import net.sample.dataloadorchestrator.model.DataLoadSummary;
import net.sample.dataloadorchestrator.model.FileSummary;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.*;
import net.sample.dataloadorchestrator.response.BuildSummary;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BuildServiceTest {
    private static final String BUILD = "Build";
    private static final String ENTITY = "Email";
    private static final String FILENAME = "Filename.csv";
    
    @Mock
    private BuildSummaryMapper buildInfoMapper;
    
    @Mock
    private DataLoadSummaryMapper dataLoadHistoryMapper;
    
    @Mock
    private BuildInfoRepository buildRepository;

    @Mock
    private DataLoadHistoryRepository dataLoadHistoryRepository;

    @InjectMocks
    private BuildService service;

    @Test
    void testGetAllBuildSummaries() {
        List<BuildInfo> buildInfos = Collections.emptyList();
        when(buildRepository.findAll())
                .thenReturn(buildInfos);
        when(buildInfoMapper.map(anyList()))
                .thenReturn(List.of(getSummary()));

        List<BuildSummary> result = service.getAllBuildSummaries();

        verify(buildRepository, times(1)).findAll();
        verifyNoInteractions(dataLoadHistoryMapper);
        assertEquals(1, result.size());
    }
    
    @Test
    void testGetBuildSummary() {
        BuildInfo info = new BuildInfo();
        info.setLoadHistories(Collections.emptyList());
        DataLoadSummary loadSummary = new DataLoadSummary();
        loadSummary.setBuild(BUILD);
        when(buildRepository.findById(BUILD))
                .thenReturn(Optional.of(info));
        when(buildInfoMapper.map(any(BuildInfo.class)))
                .thenReturn(getSummary());
        when(dataLoadHistoryMapper.map(anyList()))
                .thenReturn(List.of(loadSummary));

        BuildSummary result = service.getBuildSummary(BUILD);

        assertNotNull(result);
        assertNotNull(result.getEntities());
        assertEquals(BUILD, result.getEntities().get(0).getBuild());
    }

    @Test
    void testGetEntityFiles() {
        DataLoadHistory info = new DataLoadHistory();
        DataLoadHistoryId id = new DataLoadHistoryId(BUILD, ENTITY);
        DataLoadSummary loadSummary = new DataLoadSummary();
        loadSummary.setBuild(BUILD);
        FileSummary fileSummary = new FileSummary();
        fileSummary.setPath("filePath");
        loadSummary.setFiles(List.of(fileSummary));
        when(dataLoadHistoryRepository.findById(id))
                .thenReturn(Optional.of(info));
        when(dataLoadHistoryMapper.map(any(DataLoadHistory.class)))
                .thenReturn(loadSummary);

        DataLoadSummary result = service.getEntityFiles(BUILD, ENTITY, null);
        assertNotNull(result);
        assertNotNull(result.getFiles());
    }

    @Test
    void getEntityFilesByFileName() {
        DataLoadSummary dataLoadSummary = new DataLoadSummary();
        dataLoadSummary.setBuild(BUILD);
        FileSummary fileSummary = new FileSummary();
        fileSummary.setPath(String.format("BUILD=%s/%s/%s", BUILD, "electronic_addresses", FILENAME));
        dataLoadSummary.setFiles(List.of(fileSummary));

        DataLoadSummary result = service.getEntityFilesByFileName(
                BUILD,
                "email",
                FILENAME,
                dataLoadSummary);
        assertNotNull(result);
        assertNotNull(result.getFiles());
    }

    @Test
    void testGetBuildSummary_Null() {
        when(buildRepository.findById(BUILD))
                .thenReturn(Optional.empty());

        BuildSummary result = service.getBuildSummary(BUILD);
        assertNull(result);
    }

    private BuildSummary getSummary() {
        return BuildSummary.builder()
            .build(BUILD)
            .completeFileAvailable(Boolean.TRUE)
            .state(DataLoadState.LOADING)
            .build();
    }
}
