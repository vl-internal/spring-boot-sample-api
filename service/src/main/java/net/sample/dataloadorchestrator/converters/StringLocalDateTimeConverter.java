package net.sample.dataloadorchestrator.converters;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class StringLocalDateTimeConverter extends StdConverter<LocalDateTime, String> {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    @Override
    public String convert(LocalDateTime value) {
        final ZonedDateTime zDateTime = ZonedDateTime.of(value, ZoneId.systemDefault());
        return zDateTime.format(FORMATTER);
    }

}
