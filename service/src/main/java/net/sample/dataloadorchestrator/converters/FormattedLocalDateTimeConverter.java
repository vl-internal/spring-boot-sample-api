package net.sample.dataloadorchestrator.converters;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class FormattedLocalDateTimeConverter extends StdConverter<String, LocalDateTime> {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX");

    @Override
    public LocalDateTime convert(String value) {
        return LocalDateTime.parse(value, FORMATTER).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
