package net.sample.dataloadorchestrator.converters;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class LocalDateTimeConverter extends StdConverter<String, LocalDateTime> {

    @Override
    public LocalDateTime convert(String value) {
        return Instant.parse(value).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

}
