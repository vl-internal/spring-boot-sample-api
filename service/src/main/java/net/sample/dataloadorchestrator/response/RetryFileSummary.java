package net.sample.dataloadorchestrator.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RetryFileSummary {
    private Boolean acknowledge;
    private String reason;
    private String filename;
}
