package net.sample.dataloadorchestrator.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sample.dataloadorchestrator.converters.FormattedLocalDateTimeConverter;
import net.sample.dataloadorchestrator.converters.StringLocalDateTimeConverter;
import net.sample.dataloadorchestrator.model.DataLoadState;
import net.sample.dataloadorchestrator.model.DataLoadSummary;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BuildSummary {
    private String build;
    private DataLoadState state;
    private boolean completeFileAvailable;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    @JsonSerialize(converter = StringLocalDateTimeConverter.class)
    @JsonDeserialize(converter = FormattedLocalDateTimeConverter.class)
    private LocalDateTime completedDate;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private List<DataLoadSummary> entities;
}
