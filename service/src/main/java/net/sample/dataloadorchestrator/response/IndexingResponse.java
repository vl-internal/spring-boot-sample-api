package net.sample.dataloadorchestrator.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.sample.dataloadorchestrator.model.DataLoadSummary;

import java.util.List;

@Data
@AllArgsConstructor
public class IndexingResponse {
    List<DataLoadSummary> summaries;
    boolean indexingInProgress;
}
