package net.sample.dataloadorchestrator.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RetryFileResponse {
    private String build;
    private String entity;
    private List<RetryFileSummary> retryFileSummaryList = new ArrayList<>();
}
