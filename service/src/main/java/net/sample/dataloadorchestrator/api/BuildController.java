package net.sample.dataloadorchestrator.api;

import net.sample.dataloadorchestrator.model.DataLoadSummary;
import net.sample.dataloadorchestrator.model.FileSummary;
import net.sample.dataloadorchestrator.response.BuildSummary;
import net.sample.dataloadorchestrator.response.IndexingResponse;
import net.sample.dataloadorchestrator.service.BuildService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import net.sample.dataloadorchestrator.constants.HTTPResponseConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@AllArgsConstructor
@RestController
@Tag(name = "Build Information")
@RequestMapping("/api/builds")
public class BuildController {
    private final BuildService buildService;


    @Operation(
            summary = "Gets the list of all builds available",
            responses = {
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_OK_CODE,
                            description = "List of builds or empty if there are no records."
                    ),
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_CODE,
                            description = "The server is unable to process the request due to an error",
                            content = @Content)
            })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<BuildSummary>> getBuilds() {
        final List<BuildSummary> results = buildService.getAllBuildSummaries();
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @Operation(
            summary = "Gets an specific build and the files related",
            responses = {
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_OK_CODE,
                            description = "Specific build and the files related or empty if there are no records."
                    ),
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_NOT_FOUND_CODE,
                            description = "The specified build does not exists"
                    ),
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_CODE,
                            description = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_DESC,
                            content = @Content
                    )
            })
    @GetMapping(path = "/{buildName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BuildSummary> getBuild(@PathVariable("buildName") final String buildName) {
        final BuildSummary buildSummaryResult = buildService.getBuildSummary(buildName);
        if (Objects.isNull(buildSummaryResult)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(buildSummaryResult, HttpStatus.OK);
    }

    @Operation(
            summary = "Gets a file information from a specific entity and build",
            responses = {
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_OK_CODE,
                            description = "Specific file information related or empty if there are no records."
                    ),
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_NOT_FOUND_CODE,
                            description = "The specified fileId does not exists"
                    ),
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_CODE,
                            description = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_DESC,
                            content = @Content
                    )
            })
    @GetMapping(path = "/{buildName}/entities/{entity}/files/{fileId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FileSummary> getEntityFileSummary(@PathVariable("buildName") final String buildName,
                                                            @PathVariable("entity") final String entity,
                                                            @PathVariable("fileId") final UUID fileId) {
        final FileSummary fileSummaryResult = buildService.getFileSummary(buildName, entity, fileId);
        if (Objects.isNull(fileSummaryResult)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(fileSummaryResult, HttpStatus.OK);
    }

    @Operation(
            summary = "Gets a list of files from an entity",
            responses = {
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_OK_CODE,
                            description = "Specific files information related or empty if there are no records."
                    ),
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_NOT_FOUND_CODE,
                            description = "The specified entity does not exists"
                    ),
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_CODE,
                            description = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_DESC,
                            content = @Content
                    )
            })
    @GetMapping(path = "/{buildName}/entities/{entity}/files", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DataLoadSummary> getEntityFiles(@PathVariable("buildName") final String buildName,
                                                          @PathVariable("entity") final String entity,
                                                          @RequestParam(name = "fileName", required = false) String fileName) {
        final DataLoadSummary dataLoadSummaryResult = buildService.getEntityFiles(buildName, entity, fileName);
        if (Objects.isNull(dataLoadSummaryResult)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dataLoadSummaryResult, HttpStatus.OK);
    }

    @Operation(
            summary = "Gets an list of entities that are indexing",
            responses = {
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_OK_CODE,
                            description = "List of entities that are indexing or empty if there are none."
                    ),
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_CODE,
                            description = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_DESC,
                            content = @Content
                    )
            })
    @GetMapping(path = "/indexing", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IndexingResponse> getBuild() {
        final List<DataLoadSummary> results = buildService.getIndexingBuildSummaries();
        final IndexingResponse response = new IndexingResponse(results, !results.isEmpty());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
