package net.sample.dataloadorchestrator.api;

import net.sample.dataloadorchestrator.response.DataLoadSettingSummary;
import net.sample.dataloadorchestrator.service.DataLoadSettingsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sample.dataloadorchestrator.constants.HTTPResponseConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@RestController
@Tag(name="Data Load Settings")
@RequestMapping("/api")
@Slf4j
public class DataLoadSettingsController {

    private final DataLoadSettingsService dataLoadSettingsService;

    @Operation(
            summary = "Gets the list of load settings for the data load entities",
            responses = {
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_OK_CODE,
                            description = "List of entity load settings or empty if there are no records."
                    ),
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_CODE,
                            description = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_DESC,
                            content = @Content
                    )
            })
    @GetMapping(path = "/load-settings", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DataLoadSettingSummary>> getLoadSettings() {
        List<DataLoadSettingSummary> mappedSettings = dataLoadSettingsService.getDataLoadSettingSummaries();
        return new ResponseEntity<>(mappedSettings, HttpStatus.OK);
    }


    @Operation(
            summary = "Updates the data load setting for a given entity.",
            responses = {
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_OK_CODE,
                            description = "Update was successful and returns the list of updated entity load settings."
                    ),
                    @ApiResponse(
                            responseCode = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_CODE,
                            description = HTTPResponseConstants.HTTP_INTERNAL_SERVER_ERROR_DESC,
                            content = @Content
                    )
            })
    @PostMapping(path = "/load-settings", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DataLoadSettingSummary>> setLoadSettings(@RequestBody DataLoadSettingSummary summary) {
        try {
             dataLoadSettingsService.saveDataLoadSetting(summary);
        } catch (IllegalArgumentException exception) {
            log.error("Unable to save entity load setting. Invalid entity type [{}] provided. Exception : {}", summary.getEntity(), exception.getMessage());
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        //Return updated list as a convenience
        return new ResponseEntity<>(dataLoadSettingsService.getDataLoadSettingSummaries(), HttpStatus.OK);
    }
}
