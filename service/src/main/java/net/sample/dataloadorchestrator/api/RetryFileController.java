package net.sample.dataloadorchestrator.api;

import net.sample.dataloadorchestrator.model.RetryFileRequest;
import net.sample.dataloadorchestrator.response.RetryFileResponse;
import net.sample.dataloadorchestrator.service.RetryFileInterface;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@AllArgsConstructor
@RestController
public class RetryFileController {
    protected static final String RETRY_FILE_ENDPOINT = "/api/retry-files";
    private final RetryFileInterface retryFileService;

    @Operation(
            summary = "Retry file API",
            description = ".",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successfully processed files"
                    ),
                    @ApiResponse(
                            responseCode = "415",
                            description = "Unsupported Media Type"
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Not found"
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error"
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad Request",
                            content = @Content
                    )
            })
    @ResponseBody
    @PostMapping(value = RETRY_FILE_ENDPOINT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RetryFileResponse retryFiles(@RequestBody @Valid RetryFileRequest retryFileRequest) {
        return retryFileService.retryFiles(retryFileRequest);
    }

}
