package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import net.sample.dataloadorchestrator.model.DataLoadState;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "data_load_info")
public class DataLoadInfo {
    @Id
    @GeneratedValue
    private UUID id;
    @Column(insertable = false, updatable = false)
    private String build;
    @Column(insertable = false, updatable = false)
    private String entity;
    private String bucket;
    private String filePath;
    private long fileSize;
    @Enumerated(EnumType.STRING)
    private DataLoadState state;
    private LocalDateTime fileCreatedDate;

    @CreationTimestamp
    private LocalDateTime dateCreated;
    @UpdateTimestamp
    private LocalDateTime dateUpdated;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "build", referencedColumnName = "build"),
            @JoinColumn(name = "entity", referencedColumnName = "entity")
    })
    private DataLoadHistory loadHistory;
}
