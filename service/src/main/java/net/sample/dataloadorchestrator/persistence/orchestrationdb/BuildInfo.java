package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;


@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "build_info")
public class BuildInfo {
    @Id
    private String build;
    private boolean completeFilePresent;

    private LocalDateTime completedFileDate;

    @OneToMany(mappedBy = "buildInfo", fetch = FetchType.LAZY)
    private List<DataLoadHistory> loadHistories;

    public BuildInfo(String build, boolean completeFilePresent, LocalDateTime completedFileDate) {
        this.build = build;
        this.completeFilePresent = completeFilePresent;
        this.completedFileDate = completedFileDate;
    }
}
