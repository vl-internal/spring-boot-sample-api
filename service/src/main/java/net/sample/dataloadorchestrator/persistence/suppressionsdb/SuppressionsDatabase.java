package net.sample.dataloadorchestrator.persistence.suppressionsdb;

import net.sample.dataloadorchestrator.persistence.BaseDatabase;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
public class SuppressionsDatabase extends BaseDatabase {
    public SuppressionsDatabase(ResourceLoader resourceLoader, @Qualifier("suppressionDataSource") HikariDataSource dataSource) {
        super(resourceLoader, dataSource);
    }

    @Override
    @Async("suppressionExecutor")
    public CompletableFuture<Boolean> executeQueryAsync(String query) {
        return super.executeQueryAsync(query);
    }

}
