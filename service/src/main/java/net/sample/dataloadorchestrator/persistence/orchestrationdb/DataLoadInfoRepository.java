package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface DataLoadInfoRepository extends JpaRepository<DataLoadInfo, UUID> {
    DataLoadInfo findTopByBuildAndEntityAndFilePath(final String build, final String entity, final String filePath);
    Optional<DataLoadInfo> findByBuildAndEntityAndId(final String build, final String entity, final UUID fileId);
}
