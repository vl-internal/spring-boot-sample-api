package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class DataLoadHistoryId implements Serializable {
    private String build;
    private String entity;
}
