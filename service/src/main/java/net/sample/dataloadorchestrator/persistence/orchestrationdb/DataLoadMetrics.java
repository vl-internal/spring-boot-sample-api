package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@NoArgsConstructor
@Table(name = "data_load_metrics")
public class DataLoadMetrics {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private UUID fileInfoId ; // references data_load_info id
    private int recordsSucceeded;
    private int recordsFailed;

    public DataLoadMetrics(UUID fileInfoId, int recordsSucceeded, int recordsFailed) {
        this.fileInfoId = fileInfoId;
        this.recordsSucceeded = recordsSucceeded;
        this.recordsFailed = recordsFailed;
    }
}
