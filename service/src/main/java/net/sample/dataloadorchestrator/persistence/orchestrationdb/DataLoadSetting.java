package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "data_load_settings")
public class DataLoadSetting {

    @Id
    private String entity;

    @Column(name = "enable_loading")
    private Boolean loadingEnabled;
}
