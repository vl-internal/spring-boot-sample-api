package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import lombok.Getter;
import lombok.Setter;
import net.sample.dataloadorchestrator.model.DataLoadState;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "data_load_history")
@IdClass(DataLoadHistoryId.class)
public class DataLoadHistory {
    @Id
    @Column(insertable = false, updatable = false)
    private String build;
    @Id
    private String entity;
    @Column(updatable = false)
    private String entityTargetColor;
    @Enumerated(EnumType.STRING)
    private DataLoadState state;
    @Column(updatable = false, insertable = false)
    private long deliveredFiles;
    @Column(updatable = false, insertable = false)
    private long successfulFiles;
    @Column(updatable = false, insertable = false)
    private long failedFiles;
    @CreationTimestamp
    private LocalDateTime startedDate;
    private LocalDateTime finishedDate;

    @OneToMany(mappedBy = "loadHistory", fetch = FetchType.LAZY)
    private List<DataLoadInfo> files;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "build", referencedColumnName = "build", insertable = false)
    private BuildInfo buildInfo;

}
