package net.sample.dataloadorchestrator.persistence;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class BaseDatabase {
    public static final String SCRIPTS_FOLDER = "scripts/";
    private final ResourceLoader resourceLoader;
    private final HikariDataSource dataSource;

    public BaseDatabase(final ResourceLoader resourceLoader, final HikariDataSource dataSource) {
        this.resourceLoader = resourceLoader;
        this.dataSource = dataSource;
    }

    public String loadQuery(final String location) throws IOException {
        final var resourceLocation = ResourceUtils.CLASSPATH_URL_PREFIX + SCRIPTS_FOLDER + location;
        final var resource = resourceLoader.getResource(resourceLocation);
        try (var inputStream = resource.getInputStream()) {
            final var lines = IOUtils.readLines(inputStream, Charset.defaultCharset());

            return String.join(" ", lines);
        }
    }

    public boolean executeQuery(final String query) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.execute();
            return true;
        } catch (final SQLException ex) {
            log.error("Something happened while executing query", ex);
            return false;
        }
    }

    public CompletableFuture<Boolean> executeQueryAsync(final String query) {
        return CompletableFuture.completedFuture(executeQuery(query));
    }
}
