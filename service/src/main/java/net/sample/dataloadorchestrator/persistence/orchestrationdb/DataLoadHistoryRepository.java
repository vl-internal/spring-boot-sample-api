package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import net.sample.dataloadorchestrator.model.DataLoadState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface DataLoadHistoryRepository extends JpaRepository<DataLoadHistory, DataLoadHistoryId> {

    String INCREASE_DELIVERED_COUNTER =
            "UPDATE data_load_history SET delivered_files = delivered_files + 1 WHERE build = :build AND entity = :entity";

    String INCREASE_SUCCESS_COUNTER =
            "UPDATE data_load_history SET successful_files = successful_files + 1 WHERE build = :build AND entity = :entity";

    String INCREASE_FAIL_COUNTER =
            "UPDATE data_load_history SET failed_files = failed_files + 1 WHERE build = :build AND entity = :entity";

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = INCREASE_DELIVERED_COUNTER, nativeQuery = true)
    void increaseDelivered(final @Param("build") String build, final @Param("entity") String entity);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = INCREASE_SUCCESS_COUNTER, nativeQuery = true)
    void increaseSuccesses(final @Param("build") String build, final @Param("entity") String entity);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = INCREASE_FAIL_COUNTER, nativeQuery = true)
    void increaseFailures(final @Param("build") String build, final @Param("entity") String entity);

    List<DataLoadHistory> findByState(final DataLoadState state);

    long countByState(final DataLoadState state);
}
