package net.sample.dataloadorchestrator.persistence.orchestrationdb;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataLoadMetricsRepository extends JpaRepository<DataLoadMetrics, Long> {
}
