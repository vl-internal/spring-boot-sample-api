package net.sample.dataloadorchestrator.properties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "data-load")
public class DataLoadProperties {
    private long maxIndexesRestore;
    private long timeAfterCompleteFileAppeared;
}
