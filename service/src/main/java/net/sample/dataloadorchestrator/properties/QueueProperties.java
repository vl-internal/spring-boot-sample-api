package net.sample.dataloadorchestrator.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueueProperties {
    private String topicName;
    private String subscriptionName;
    private String projectId;
}
