package net.sample.dataloadorchestrator.properties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "cassandra")
public class CassandraProperties {
    private String host;
    private Integer port;
    private String localDataCenter;
    private String user;
    private String password;
    private String keySpace;
    private Boolean ssl;
    private Long requestTimeout;
}
