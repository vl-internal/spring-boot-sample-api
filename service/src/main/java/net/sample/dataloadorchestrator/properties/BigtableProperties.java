package net.sample.dataloadorchestrator.properties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "bigtable")
public class BigtableProperties {
    private String instanceId;
    private String tableId;
    private String columnFamily;
    private String deployment;
    private String prefixSeparator;
    private int emulatorPort;
    private String managementEntity;
}
