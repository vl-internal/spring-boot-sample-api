package net.sample.dataloadorchestrator.properties;

import org.springframework.boot.autoconfigure.condition.AnyNestedCondition;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

public class RetryFileCondition extends AnyNestedCondition {
    public RetryFileCondition() {
        super(ConfigurationPhase.REGISTER_BEAN);
    }

    @ConditionalOnProperty(name = {"environment.provider"}, havingValue = "gcp")
    static class RetryFileConditionGCP {}

    @ConditionalOnProperty(name = {"environment.provider"}, havingValue = "local")
    static class RetryFileConditionLocal {}
}
