package net.sample.dataloadorchestrator.model;

import lombok.Getter;

@Getter
public enum DataLoadEntityInfo {
    ABCD(DataLoadEntity.ABCD, "Abcd", DataLoadEntityTarget.IDENTITY_RESOLUTION),
    ELECTRONIC_ADDRESSES(DataLoadEntity.EMAIL, "Email", DataLoadEntityTarget.IDENTITY_RESOLUTION),
    NON_EXISTENT(null, null, null);

    private final DataLoadEntity dataLoadEntity;
    private final String entityName;
    private final DataLoadEntityTarget dataLoadEntityTarget;

    DataLoadEntityInfo(DataLoadEntity dataLoadEntity, String entityName, DataLoadEntityTarget dataLoadEntityTarget) {
        this.dataLoadEntity = dataLoadEntity;
        this.entityName = entityName;
        this.dataLoadEntityTarget = dataLoadEntityTarget;
    }

    public static DataLoadEntityInfo fromFolderName(String folderName) {
        for (var value : DataLoadEntityInfo.values()) {
            if (value.name().equalsIgnoreCase(folderName)) {
                return value;
            }
        }
        return DataLoadEntityInfo.NON_EXISTENT;
    }

    public static DataLoadEntityInfo fromEntityName(String entityName) {
        for (var value : DataLoadEntityInfo.values()) {
            if (value.getEntityName() != null && value.entityName.equalsIgnoreCase(entityName)) {
                return value;
            }
        }
        return DataLoadEntityInfo.NON_EXISTENT;
    }

    public static String getFolderName(String entityName) {
        for(var value : DataLoadEntityInfo.values()) {
            if (value.getEntityName() != null && value.entityName.equalsIgnoreCase(entityName)) {
                return value.name();
            }
        }
        return null;
    }

}
