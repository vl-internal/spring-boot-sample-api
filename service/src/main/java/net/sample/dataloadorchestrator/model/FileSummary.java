package net.sample.dataloadorchestrator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sample.dataloadorchestrator.converters.FormattedLocalDateTimeConverter;
import net.sample.dataloadorchestrator.converters.StringLocalDateTimeConverter;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileSummary {
    private String bucket;
    private String path;
    private long sizeInBytes;
    private DataLoadState state;
    @JsonSerialize(converter = StringLocalDateTimeConverter.class)
    @JsonDeserialize(converter = FormattedLocalDateTimeConverter.class)
    private LocalDateTime startDate;
    @JsonSerialize(converter = StringLocalDateTimeConverter.class)
    @JsonDeserialize(converter = FormattedLocalDateTimeConverter.class)
    private LocalDateTime lastUpdatedDate;
}
