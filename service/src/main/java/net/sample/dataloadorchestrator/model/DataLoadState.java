package net.sample.dataloadorchestrator.model;

public enum DataLoadState {
    DETECTED,
    DONE,
    FAILED,
    FAILED_RESTORING_INDEXES,
    LOADING,
    LOAD_STARTED,
    LOAD_COMPLETED,
    LOAD_INCOMPLETE,
    RESTORING_INDEXES
}
