package net.sample.dataloadorchestrator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sample.dataloadorchestrator.converters.FormattedLocalDateTimeConverter;
import net.sample.dataloadorchestrator.converters.StringLocalDateTimeConverter;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataLoadSummary {
    private String build;
    private String entity;
    private String targetColor;
    private DataLoadState state;
    private long numberOfFiles;
    private long deliveredFiles;
    private long successfulFiles;
    private long failedFiles;
    @JsonSerialize(converter = StringLocalDateTimeConverter.class)
    @JsonDeserialize(converter = FormattedLocalDateTimeConverter.class)
    private LocalDateTime startedDate;
    @JsonSerialize(converter = StringLocalDateTimeConverter.class)
    @JsonDeserialize(converter = FormattedLocalDateTimeConverter.class)
    private LocalDateTime completedDate;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<FileSummary> files;
}
