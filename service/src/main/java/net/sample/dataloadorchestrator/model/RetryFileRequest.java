package net.sample.dataloadorchestrator.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RetryFileRequest {
    @NotBlank
    private String build;
    @NotBlank
    private String entity;
    @NotNull
    @Size(min = 1)
    private List<String> filenames;

}
