package net.sample.dataloadorchestrator.model;

public enum DataLoadEntityTarget {
    IDENTITY_RESOLUTION,
    NO_SQL,
    SUPPRESSIONS,
    NOWHERE;
}
