package net.sample.dataloadorchestrator.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sample.dataloadorchestrator.converters.LocalDateTimeConverter;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RetryFileServiceBus {
    private String bucket;
    private String name;
    private long size;
    @JsonDeserialize(converter = LocalDateTimeConverter.class)
    private LocalDateTime timeCreated;
}
