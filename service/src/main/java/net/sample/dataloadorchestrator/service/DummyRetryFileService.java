package net.sample.dataloadorchestrator.service;

import lombok.extern.slf4j.Slf4j;
import net.sample.dataloadorchestrator.model.RetryFileRequest;
import net.sample.dataloadorchestrator.response.RetryFileResponse;
import net.sample.dataloadorchestrator.response.RetryFileSummary;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Slf4j
@Service
public class DummyRetryFileService implements RetryFileInterface {
    @Override
    public RetryFileResponse retryFiles(RetryFileRequest retryFileRequest) {
        return new RetryFileResponse(
                retryFileRequest.getBuild(),
                retryFileRequest.getEntity(),
                retryFileRequest.getFilenames().stream()
                        .map(s ->
                                new RetryFileSummary(
                                        Boolean.TRUE,
                                        "OK",
                                        s
                                )
                        )
                        .collect(Collectors.toList())
        );
    }
}
