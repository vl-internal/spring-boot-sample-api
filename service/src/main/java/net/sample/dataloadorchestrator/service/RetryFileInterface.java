package net.sample.dataloadorchestrator.service;

import net.sample.dataloadorchestrator.model.RetryFileRequest;
import net.sample.dataloadorchestrator.response.RetryFileResponse;

public interface RetryFileInterface {
    RetryFileResponse retryFiles(RetryFileRequest retryFileRequest);
}
