package net.sample.dataloadorchestrator.service;

import lombok.AllArgsConstructor;
import net.sample.dataloadorchestrator.mapper.BuildSummaryMapper;
import net.sample.dataloadorchestrator.mapper.DataLoadSummaryMapper;
import net.sample.dataloadorchestrator.mapper.FileSummaryMapper;
import net.sample.dataloadorchestrator.model.DataLoadEntityInfo;
import net.sample.dataloadorchestrator.model.DataLoadState;
import net.sample.dataloadorchestrator.model.DataLoadSummary;
import net.sample.dataloadorchestrator.model.FileSummary;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.*;
import net.sample.dataloadorchestrator.response.BuildSummary;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class BuildService {
    private BuildSummaryMapper buildInfoMapper;
    private DataLoadSummaryMapper dataLoadHistoryMapper;
    private BuildInfoRepository buildRepository;
    private DataLoadHistoryRepository dataLoadHistoryRepository;
    private DataLoadInfoRepository dataLoadInfoRepository;
    private FileSummaryMapper fileSummaryMapper;

    public List<BuildSummary> getAllBuildSummaries() {
        List<BuildInfo> builds = buildRepository.findAll();
        return buildInfoMapper.map(builds);
    }

    public BuildSummary getBuildSummary(String buildName) {
        Optional<BuildInfo> buildOpt = buildRepository.findById(buildName);
        if (buildOpt.isEmpty()) {
            return null;
        }
        BuildInfo buildInfo = buildOpt.get();
        BuildSummary buildSummary = buildInfoMapper.map(buildInfo);
        List<DataLoadSummary> loadHistories = dataLoadHistoryMapper.map(buildInfo.getLoadHistories());
        buildSummary.setEntities(loadHistories);
        return buildSummary;
    }

    public DataLoadSummary getEntityFiles(String buildName, String entity, String fileName) {
        DataLoadHistoryId id = new DataLoadHistoryId(buildName, entity);
        Optional<DataLoadHistory> dataLoadHistoryOpt = dataLoadHistoryRepository.findById(id);
        if (dataLoadHistoryOpt.isEmpty()) {
            return null;
        }
        DataLoadHistory dataLoadHistory = dataLoadHistoryOpt.get();
        DataLoadSummary dataLoadSummary = dataLoadHistoryMapper.map(dataLoadHistory);
        if (StringUtils.isNotEmpty(fileName)) {
            return getEntityFilesByFileName(buildName, entity, fileName, dataLoadSummary);
        }
        return dataLoadSummary;
    }

    public DataLoadSummary getEntityFilesByFileName(String buildName, String entity, String fileName, DataLoadSummary dataLoadSummary) {
        String folderName = DataLoadEntityInfo.getFolderName(entity);
        if (StringUtils.isEmpty(folderName)) {
            return null;
        }
        String filePath  = String.format("BUILD=%s/%s/%s", buildName, folderName, fileName);
        List<FileSummary> fileSummary = dataLoadSummary.getFiles().stream()
                .filter(e -> e.getPath().equalsIgnoreCase(filePath))
                .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(fileSummary)) {
            return null;
        }
        dataLoadSummary.setFiles(fileSummary);
        return dataLoadSummary;
    }

    public FileSummary getFileSummary(String buildName, String entity, UUID fileId) {
        Optional<DataLoadInfo> dataLoadInfoOpt = dataLoadInfoRepository.findByBuildAndEntityAndId(buildName, entity, fileId);
        if (dataLoadInfoOpt.isEmpty()) {
            return null;
        }
        DataLoadInfo dataLoadInfo = dataLoadInfoOpt.get();
        return fileSummaryMapper.map(dataLoadInfo);
    }

    public List<DataLoadSummary> getIndexingBuildSummaries() {
        List<DataLoadHistory> histories = dataLoadHistoryRepository.findByState(DataLoadState.RESTORING_INDEXES);
        return dataLoadHistoryMapper.map(histories);
    }

}
