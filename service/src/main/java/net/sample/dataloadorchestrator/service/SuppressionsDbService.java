package net.sample.dataloadorchestrator.service;

import net.sample.dataloadorchestrator.persistence.suppressionsdb.SuppressionsDatabase;
import org.springframework.stereotype.Service;

@Service
public class SuppressionsDbService extends BaseDatabaseService{
    public SuppressionsDbService(final SuppressionsDatabase targetDatabase) {
        super(targetDatabase);
    }
}
