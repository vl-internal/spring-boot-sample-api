package net.sample.dataloadorchestrator.service;

import net.sample.dataloadorchestrator.mapper.DataLoadSettingsMapper;
import net.sample.dataloadorchestrator.model.DataLoadEntity;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.DataLoadSetting;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.DataLoadSettingsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sample.dataloadorchestrator.response.DataLoadSettingSummary;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class DataLoadSettingsService {
    private final DataLoadSettingsMapper mapper;
    private final DataLoadSettingsRepository dataLoadSettingsRepository;

    public List<DataLoadSettingSummary> getDataLoadSettingSummaries() {
        var settings = dataLoadSettingsRepository.findAll();
        return mapper.map(settings);
    }


    public void saveDataLoadSetting(final DataLoadSettingSummary summary) {
        DataLoadEntity entity = DataLoadEntity.valueOf(summary.getEntity());

        DataLoadSetting setting = new DataLoadSetting();
        setting.setEntity(entity.name());
        setting.setLoadingEnabled(summary.getLoadingEnabled());
        dataLoadSettingsRepository.saveAndFlush(setting);

    }
}
