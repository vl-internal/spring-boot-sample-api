package net.sample.dataloadorchestrator.service;

import net.sample.dataloadorchestrator.model.DataLoadEntityInfo;
import net.sample.dataloadorchestrator.model.DataLoadEntityTarget;
import net.sample.dataloadorchestrator.persistence.BaseDatabase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Slf4j
@RequiredArgsConstructor
public class BaseDatabaseService {

    @Value("${database-scripts.pre-load-search-path:default/pre-load}")
    private String[] PRE_LOAD_PATHS = new String[0];
    @Value("${database-scripts.post-load-search-path:default/post-load}")
    private String[] POST_LOAD_PATHS = new String[0];

    private static final String DEFAULT_PRE_LOAD_LOCATION_FORMAT = "default/pre-load/%s.sql";
    private static final String DEFAULT_POST_LOAD_LOCATION_FORMAT = "default/post-load/%s.sql";

    private static final String COLOR_PLACEHOLDER = "${color}";

    private final BaseDatabase targetDatabase;

    public boolean dropTableIndexes(final DataLoadEntityInfo entityInfo, final String color)
            throws IOException {
        if (entityInfo.getDataLoadEntityTarget() == DataLoadEntityTarget.NOWHERE) {
            return true;
        }
        final String entity = entityInfo.getDataLoadEntity().name().toLowerCase(Locale.US);
        final String queryString = targetDatabase.loadQuery(getPreLoadFileLocation(entity))
                .replace(COLOR_PLACEHOLDER, color);
        return targetDatabase.executeQuery(queryString);
    }

    public CompletableFuture<Boolean> restoreTableIndexes(final DataLoadEntityInfo entityInfo, final String color) {
        if (entityInfo.getDataLoadEntityTarget() == DataLoadEntityTarget.NOWHERE) {
            return CompletableFuture.completedFuture(true);
        }
        log.info("Restoring indexes for '{}' with target color '{}'",
                entityInfo.getEntityName(),
                color);

        final String entity = entityInfo.getDataLoadEntity().name().toLowerCase(Locale.US);
        try {
            final String restoreIndexesQuery = targetDatabase.loadQuery(getPostLoadFileLocation(entity))
                    .replace(COLOR_PLACEHOLDER, color);
            return targetDatabase.executeQueryAsync(restoreIndexesQuery);
        } catch (final IOException ex) {
            log.error("Could not load post load query for entity '{}' and color '{}'",
                    entityInfo.getEntityName(),
                    color, ex);
            return CompletableFuture.failedFuture(ex);
        }
    }

    private String getPostLoadFileLocation(String entityName) {
        final String defaultPath =  String.format(DEFAULT_POST_LOAD_LOCATION_FORMAT, entityName);
        return selectResourcePath(POST_LOAD_PATHS, entityName).orElse(defaultPath);

    }

    private String getPreLoadFileLocation(String entityName) {
        final String defaultPath =  String.format(DEFAULT_PRE_LOAD_LOCATION_FORMAT, entityName);
        return selectResourcePath(PRE_LOAD_PATHS, entityName).orElse(defaultPath);
    }

    private Optional<String> selectResourcePath(final String[] searchPaths, final String entityName) {
        for(String searchPath : searchPaths) {
            final String resourcePath = searchPath + "/" + entityName.toLowerCase(Locale.US) + ".sql";
            if (resourceExists(resourcePath)) {
                return Optional.of(resourcePath);
            }
        }
        return Optional.empty();
    }

    private boolean resourceExists(String resourcePath) {
        final var resourceLocation = ResourceUtils.CLASSPATH_URL_PREFIX + BaseDatabase.SCRIPTS_FOLDER + resourcePath;
        log.debug("Looking for resource [{}]", resourceLocation);
        return (this.getClass().getResource(resourceLocation) != null);
    }
}

