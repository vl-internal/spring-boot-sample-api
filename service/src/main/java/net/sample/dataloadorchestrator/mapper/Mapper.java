package net.sample.dataloadorchestrator.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public interface Mapper<I, O> {
    O map(final I input);

    default List<O> map(final List<I> inputList) {
        return inputList.stream().filter(Objects::nonNull).map(this::map).collect(Collectors.toList());
    }
}
