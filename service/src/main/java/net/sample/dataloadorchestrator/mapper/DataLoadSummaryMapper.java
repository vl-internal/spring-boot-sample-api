package net.sample.dataloadorchestrator.mapper;

import net.sample.dataloadorchestrator.model.DataLoadSummary;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.DataLoadHistory;
import org.springframework.stereotype.Component;

@Component
public class DataLoadSummaryMapper implements Mapper<DataLoadHistory, DataLoadSummary> {
    
    private final FileSummaryMapper fileSummaryMapper = new FileSummaryMapper();
    
    @Override
    public DataLoadSummary map(DataLoadHistory input) {
        return DataLoadSummary.builder()
                .build(input.getBuild())
                .entity(input.getEntity())
                .state(input.getState())
                .targetColor(input.getEntityTargetColor())
                .numberOfFiles(input.getFiles().size())
                .deliveredFiles(input.getDeliveredFiles())
                .successfulFiles(input.getSuccessfulFiles())
                .failedFiles(input.getFailedFiles())
                .startedDate(input.getStartedDate())
                .completedDate(input.getFinishedDate())
                .files(fileSummaryMapper.map(input.getFiles()))
                .build();
    }

}
