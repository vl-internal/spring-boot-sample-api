package net.sample.dataloadorchestrator.mapper;

import net.sample.dataloadorchestrator.model.FileSummary;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.DataLoadInfo;
import org.springframework.stereotype.Component;

@Component
public class FileSummaryMapper implements Mapper<DataLoadInfo, FileSummary> {

    @Override
    public FileSummary map(DataLoadInfo input) {
        return FileSummary.builder()
                .bucket(input.getBucket())
                .path(input.getFilePath())
                .sizeInBytes(input.getFileSize())
                .state(input.getState())
                .startDate(input.getDateCreated())
                .lastUpdatedDate(input.getDateUpdated())
                .build();
    }

}
