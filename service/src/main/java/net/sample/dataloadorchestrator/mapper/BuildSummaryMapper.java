package net.sample.dataloadorchestrator.mapper;

import java.util.Objects;

import net.sample.dataloadorchestrator.model.DataLoadState;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.BuildInfo;
import net.sample.dataloadorchestrator.response.BuildSummary;
import org.springframework.stereotype.Component;

@Component
public class BuildSummaryMapper implements Mapper<BuildInfo, BuildSummary> {

    @Override
    public BuildSummary map(BuildInfo build) {
        DataLoadState state = Objects.isNull(build.getCompletedFileDate())
                ? DataLoadState.LOADING
                : DataLoadState.DONE;

        return BuildSummary.builder()
                .build(build.getBuild())
                .state(state)
                .completeFileAvailable(build.isCompleteFilePresent())
                .completedDate(build.getCompletedFileDate())
                .build();
    }

}
