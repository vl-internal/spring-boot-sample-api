package net.sample.dataloadorchestrator.mapper;

import net.sample.dataloadorchestrator.model.DataLoadEntity;
import net.sample.dataloadorchestrator.persistence.orchestrationdb.DataLoadSetting;
import net.sample.dataloadorchestrator.response.DataLoadSettingSummary;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.stereotype.Component;

@Component
public class DataLoadSettingsMapper implements Mapper<DataLoadSetting, DataLoadSettingSummary>{

    @Override
    public DataLoadSettingSummary map(final DataLoadSetting input) {
        if (EnumUtils.isValidEnum(DataLoadEntity.class, input.getEntity())) {
            return DataLoadSettingSummary.builder()
                    .entity(input.getEntity())
                    .loadingEnabled(input.getLoadingEnabled())
                    .build();
        }
        return null;
    }
}
