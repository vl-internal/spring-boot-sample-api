package net.sample.dataloadorchestrator.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.concurrent.Executor;

@Configuration
@EnableTransactionManagement
@ConfigurationProperties(prefix = "database.suppression")
@EnableJpaRepositories(
        basePackages = {SuppressionDBConfiguration.PACKAGE},
        entityManagerFactoryRef = SuppressionDBConfiguration.FACTORY,
        transactionManagerRef = "suppressionTransactionManager")
public class SuppressionDBConfiguration extends HikariConfig {
    public static final String PACKAGE = "net.sample.dataloadorchestrator.persistence.suppressionsdb";
    public static final String FACTORY = "suppressionEntityManagerFactory";


    @Value("${async.suppression-db.num-threads}")
    private int executorSize;

    @Bean
    public HikariDataSource suppressionDataSource() {
        return new HikariDataSource(this);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean suppressionEntityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("suppressionDataSource") HikariDataSource suppressionDataSource) {
        return builder.dataSource(suppressionDataSource)
                .packages(PACKAGE)
                .persistenceUnit("suppression").build();
    }

    @Bean
    public PlatformTransactionManager suppressionTransactionManager(
            final @Qualifier(FACTORY) EntityManagerFactory suppressionsEntityManagerFactory) {
        return new JpaTransactionManager(suppressionsEntityManagerFactory);
    }

    @Bean
    public Executor suppressionsExecutor() {
        final var executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(executorSize);
        executor.setMaxPoolSize(executorSize);
        return executor;
    }
}
