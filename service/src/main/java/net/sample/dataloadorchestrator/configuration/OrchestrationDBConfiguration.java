package net.sample.dataloadorchestrator.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;

@Configuration
@EnableTransactionManagement
@ConfigurationProperties(prefix = "database.orchestration")
@EnableJpaRepositories(
        basePackages = {OrchestrationDBConfiguration.PACKAGE},
        entityManagerFactoryRef = OrchestrationDBConfiguration.FACTORY,
        transactionManagerRef = "orchestrationTransactionManager")
public class OrchestrationDBConfiguration extends HikariConfig {
    public static final String PACKAGE = "net.sample.dataloadorchestrator.persistence.orchestrationdb";
    public static final String FACTORY = "orchestrationEntityManagerFactory";

    @Primary
    @Bean
    public HikariDataSource orchestrationDataSource() {
        return new HikariDataSource(this);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean orchestrationEntityManagerFactory(
            final EntityManagerFactoryBuilder builder,
            final HikariDataSource orchestrationDataSource) {
        return builder.dataSource(orchestrationDataSource)
                .packages(PACKAGE)
                .persistenceUnit("orchestration")
                .build();
    }

    @Bean
    public PlatformTransactionManager orchestrationTransactionManager(
            @Qualifier(FACTORY)
            EntityManagerFactory orchestrationEntityManagerFactory) {
        return new JpaTransactionManager(orchestrationEntityManagerFactory);
    }

}

