package net.sample.dataloadorchestrator.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.concurrent.Executor;

@Configuration
@EnableTransactionManagement
@ConfigurationProperties(prefix = "database.resolution")
@EnableJpaRepositories(
        basePackages = {ResolutionDBConfiguration.PACKAGE},
        entityManagerFactoryRef = ResolutionDBConfiguration.FACTORY,
        transactionManagerRef = "resolutionTransactionManager")
public class ResolutionDBConfiguration extends HikariConfig {
    public static final String PACKAGE = "net.sample.dataloadorchestrator.persistence.resolutiondb";
    public static final String FACTORY = "resolutionEntityManagerFactory";

    @Value("${async.resolution-db.num-threads}")
    private int executorSize;

    @Bean
    public HikariDataSource resolutionDataSource() {
        return new HikariDataSource(this);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean resolutionEntityManagerFactory(
            final EntityManagerFactoryBuilder builder,
            final @Qualifier("resolutionDataSource") HikariDataSource resolutionDataSource) {
        return builder.dataSource(resolutionDataSource)
                .packages(PACKAGE)
                .persistenceUnit("resolution").build();
    }

    @Bean
    public PlatformTransactionManager resolutionTransactionManager(
            final @Qualifier(FACTORY) EntityManagerFactory resolutionEntityManagerFactory) {
        return new JpaTransactionManager(resolutionEntityManagerFactory);
    }

    @Bean
    public Executor resolutionExecutor() {
        final var executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(executorSize);
        executor.setMaxPoolSize(executorSize);
        return executor;
    }
}
