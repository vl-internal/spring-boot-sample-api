package net.sample.dataloadorchestrator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataLoadOrchestratorApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataLoadOrchestratorApplication.class, args);
    }
}
