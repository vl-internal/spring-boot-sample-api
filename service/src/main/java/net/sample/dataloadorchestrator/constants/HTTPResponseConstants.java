package net.sample.dataloadorchestrator.constants;

public class HTTPResponseConstants {
    public static final String HTTP_OK_CODE = "200";
    public static final String HTTP_NOT_FOUND_CODE = "404";
    public static final String HTTP_INTERNAL_SERVER_ERROR_CODE = "500";
    public static final String HTTP_INTERNAL_SERVER_ERROR_DESC = "The server is unable to process the request due to an error.";
}
