CREATE TABLE ${schemaName}.data_load_history (
  build                    VARCHAR(256) NOT NULL,
  entity                   VARCHAR(100) NOT NULL,
  entity_target_color      VARCHAR(7)   NOT NULL,
  state                    VARCHAR(32)  NOT NULL,
  delivered_files          BIGINT NOT NULL DEFAULT 0,
  successful_files         BIGINT NOT NULL DEFAULT 0,
  failed_files             BIGINT NOT NULL DEFAULT 0,
  completed_file_available BOOLEAN NOT NULL DEFAULT FALSE,
  completed_file_date      TIMESTAMP,
  started_date             TIMESTAMP NOT NULL,
  finished_date            TIMESTAMP,

  PRIMARY KEY(build, entity)
);
