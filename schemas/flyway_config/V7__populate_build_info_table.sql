BEGIN;
INSERT INTO ${schemaName}.build_info (build, complete_file_present, completed_file_date)
SELECT dlh.build, bool_or(dlh.completed_file_available), max(completed_file_date)
FROM ${schemaName}.data_load_history dlh
WHERE dlh.build
          NOT IN (SELECT build
                  FROM ${schemaName}.build_info bi
                  WHERE bi.build = dlh.build)
GROUP BY (dlh.build);

ALTER TABLE ${schemaName}.data_load_history
    ADD CONSTRAINT build_info_fk
        FOREIGN KEY (build)
            REFERENCES ${schemaName}.build_info (build) ON DELETE CASCADE;

ALTER TABLE ${schemaName}.data_load_history
    DROP COLUMN completed_file_available,
    DROP COLUMN completed_file_date;
COMMIT;



