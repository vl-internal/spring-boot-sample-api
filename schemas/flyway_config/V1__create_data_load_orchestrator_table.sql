CREATE TABLE ${schemaName}.data_load_info
(
    id                UUID PRIMARY KEY,
    build             VARCHAR(256) NOT NULL,
    bucket            TEXT         NOT NULL,
    file_path         TEXT         NOT NULL,
    entity            VARCHAR(100) NOT NULL,
    file_size         BIGINT       NOT NULL,
    state             VARCHAR(32)  NOT NULL,
    file_created_date TIMESTAMP    NOT NULL,
    date_created      TIMESTAMP    NOT NULL,
    date_updated      TIMESTAMP    NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS dl_info_unique_file_idx ON ${schemaName}.data_load_info(build, bucket, entity, file_path);

CREATE INDEX IF NOT EXISTS dl_info_build_entity_idx ON ${schemaName}.data_load_info(build, entity);
