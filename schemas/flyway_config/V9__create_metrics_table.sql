CREATE TABLE ${schemaName}.data_load_metrics
(
    id                BIGSERIAL PRIMARY KEY,
    file_info_id      UUID REFERENCES ${schemaName}.data_load_info (id),
    records_succeeded BIGINT NOT NULL,
    records_failed    BIGINT NOT NULL
);