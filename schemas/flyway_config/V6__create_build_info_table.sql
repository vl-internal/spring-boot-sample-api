CREATE TABLE ${schemaName}.build_info
(
    build                 VARCHAR(256) PRIMARY KEY,
    complete_file_present BOOLEAN NOT NULL DEFAULT FALSE,
    completed_file_date   TIMESTAMP
);



