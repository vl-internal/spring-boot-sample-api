CREATE TABLE ${schemaName}.data_load_settings (
  entity         VARCHAR(50) NOT NULL,
  enable_loading BOOLEAN NOT NULL DEFAULT TRUE,
  PRIMARY KEY(entity)
);