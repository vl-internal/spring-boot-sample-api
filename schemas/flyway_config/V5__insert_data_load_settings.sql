-- initial data to set for entity loading - this list should match the com.wdm.dataprocessingworker.model.DataLoadEntity enum values
INSERT INTO ${schemaName}.data_load_settings (entity,enable_loading) VALUES ('ABCD',TRUE);
INSERT INTO ${schemaName}.data_load_settings (entity,enable_loading) VALUES ('EMAIL',TRUE);
