.PHONY: help

package: ## Run Maven package
	./mvnw clean package

run: ## Run Spring Boot application
	./mvnw clean package -DskipTests
	./mvnw spring-boot:run -pl service

## ======= Database commands =======
database-up: ## Start up local database
	docker run --name data-load-orch-pg -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -d -p 5435:5432 --net=local_network postgres:11
	docker run --name pg-docker -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 --net=local_network postgres:11
	docker run --name suppressions-postgres -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -d -p 5434:5432 --net=local_network postgres:11

database-down: ## Shutdown local database
	docker stop data-load-orch-pg
	docker stop pg-docker
	docker stop suppressions-postgres

database-remove:
	docker rm data-load-orch-pg
	docker rm pg-docker
	docker rm suppressions-postgres

database-migrate: ## Use flyway to create required schemas and tables
	docker run --net=local_network --rm -v $(CURDIR)/schemas/flyway_config/localhost:/flyway/conf -v $(CURDIR)/schemas:/flyway/sql flyway/flyway migrate

database-connect: ## Connect to local database
	docker exec -it -u postgres data-load-orch-pg psql

help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
