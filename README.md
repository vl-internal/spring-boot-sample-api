# data-load-orchestrator

## Description

Project in charge of orchestrating the data load process to our data sources including PostgreSQL, ...

## Run PostgreSQL database locally

### Database list:

- data_load_orchestrator
- resolution
- suppression

## Make options

| Command                              | Description                                      |
|--------------------------------------|--------------------------------------------------|
| `make run`                           | Runs the project locally                         |
| `make package`                       | Builds the client and the service                |
| `make database-up`                   | Starts the databases                             |
| `make database-migrate`              | Migrates the flyway schema to the local database |
| `make database-remove` | Remove all databases from local                  |

## Run locally

To run the project locally you have to:

- Have docker running on your computer (https://docs.docker.com/desktop/)
- Locally run dataload-orchestrator
    - Open data-load-orchestrator and run:
      - Run the makefile command `make run`
      Note: the application keeps running in this terminal.

## Swagger UI and Documentation

Swagger UI and schemas are available in all environments. An example is provided using localhost

| Resource    | URL                                         |
| ----------- | ------------------------------------------- |
| Swagger UI  | http://localhost:8085/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config |
| JSON Schema | http://localhost:8085/v3/api-docs           |
| YAML Schema | http://localhost:8085/v3/api-docs.yaml      |

